<?php
	
if(file_exists(STYLESHEETPATH.'/features/class.cpanel.php')){
 require_once(STYLESHEETPATH.'/features/class.cpanel.php');
 $themename = $WPTeamPanel->themename;
 $shortname = $WPTeamPanel->shortname;
 global $themename;
 global $shortname;
} else {
 wp_die(__('Theme init file not found!', 'kredits-online'));
}
	
function widgets_init(){
	register_sidebar( array(
		'name' => 'Боковая панель справа',
		'id' => 'widgetable-posts-right',
		'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
	
	register_sidebar( array(
		'name' => 'Панель в подвале',
		'id' => 'widgetable-posts-footer',
		'before_widget' => '<div id="%1$s" class="footer-container %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	));
};

add_action( 'widgets_init', 'widgets_init' );

add_custom_background();
add_theme_support( 'nav-menus' );
register_nav_menus( array(
	'main-menu'	=>	'Главное меню',
	'fine-menu'	=>	'Илюстрированое меню',
	'footer-menu'	=>	'Меню в подвале',
	'cities-menu'	=>	'Меню городов',
));

if (function_exists('add_theme_support')){
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size(637, 99999);
}

if(function_exists('add_image_size')){
	add_image_size('thumb-size-1', 376, 256, true);
	add_image_size('thumb-size-2', 71, 71, true);
	add_image_size('thumb-size-3', 283, 166, true);
	add_image_size('thumb-size-4', 200, 136, true);
	add_image_size('thumb-size-5', 145, 99, true);
	add_image_size('thumb-size-6', 300, 200);
}

function get_substr($text, $words=25, $echo = true){	
	$read_more_text = "...";
	$content = strip_tags($text);
	$_ = explode(" ", $content);
	$result = "";
	if(sizeOf($_)<$len)
		$len = sizeOf($_);
	for($i=0;$i<$words-1;$i++){
		$result .= $_[$i]." ";
	}
	$result .= $read_more_text;
	if(!$echo) return $result;
	return print $result;
}
	
function wp_corenavi() {
	global $wp_query, $wp_rewrite;
	$pages = '';
	$max = $wp_query->max_num_pages;
	if (!$current = get_query_var('paged')) $current = 1;
	$a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
	$a['total'] = $max;
	$a['current'] = $current;
	$total = 1;
	$a['mid_size'] = 1;
	$a['end_size'] = 3;
	$a['prev_text'] = 'Предыдущая';
	$a['next_text'] = 'Следующая';
	if ($max > 1) echo '<div class="pagination">';
	if ($total == 1 && $max > 1);
	echo $pages .paginate_links($a);
	if ($max > 1) echo '</div>';
}



add_action( 'init', 'crepashok_banks_init' );
function crepashok_banks_init() {
    $labels = array(
        'name'               => 'Отзывы о банках',
        'singular_name'      => 'Отзыв о банке',
        'menu_name'          => 'Отзывы о банках',
        'name_admin_bar'     => 'Отзывы о банках',
        'add_new'            => 'Добавить отзыв',
        'add_new_item'       => 'Добавить отзыв',
        'new_item'           => 'Новый отзыв',
        'edit_item'          => 'Редактировать отзыв',
        'view_item'          => 'Просмотр отзыва',
        'all_items'          => 'Все отзывы',
        'search_items'       => 'Поиск отзыва',
        'parent_item_colon'  => 'Родительский отзыв:',
        'not_found'          => 'Отзывы о банках не найдены.',
        'not_found_in_trash' => 'Отзывы о банках не найдены в корзине.'
    );

    $args = array(
        'labels'             => $labels,
        'description'        => 'На сайте данный тип записи не будет отображаться. ',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'bank-reviews' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 6,
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    );

    register_post_type( 'bank-reviews', $args );

    $labels = array(
        'name'               => 'Banks',
        'singular_name'      => 'Bank',
        'menu_name'          => 'Banks',
        'name_admin_bar'     => 'Bank',
        'add_new'            => 'Add New Bank',
        'add_new_item'       => 'Add New Bank',
        'new_item'           => 'New Bank',
        'edit_item'          => 'Edit Bank',
        'view_item'          => 'View Bank',
        'all_items'          => 'All Banks',
        'search_items'       => 'Search Banks',
        'parent_item_colon'  => 'Parent Banks:',
        'not_found'          => 'No banks found.',
        'not_found_in_trash' => 'No banks found in Trash.'
    );

    $args = array(
        'labels'             => $labels,
        'description'        => 'На сайте данный тип записи не будет отображаться. ',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'bank' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => 5,
        'supports'           => array( 'title', 'editor', 'thumbnail' )
    );

    register_post_type( 'bank', $args );


    $labels = array(
        'name'              => 'Разделы банков',
        'singular_name'     => 'Раздел банков',
        'search_items'      => 'Поиск раздела банков',
        'all_items'         => 'Все разделы банков',
        'parent_item'       => __( 'Parent Genre' ),
        'parent_item_colon' => __( 'Parent Genre:' ),
        'edit_item'         => __( 'Edit Genre' ),
        'update_item'       => __( 'Update Genre' ),
        'add_new_item'      => __( 'Add New Genre' ),
        'new_item_name'     => __( 'New Genre Name' ),
        'menu_name'         => 'Разделы банков',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'bank-tax' ),
    );

    register_taxonomy( 'bank-tax', array( 'bank' ), $args );
    flush_rewrite_rules();
}


?>