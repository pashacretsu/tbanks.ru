<?php /* Template Name: Landing page */ ?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta name="cmsmagazine" content="0fb1a2d032e6e4d535f6138629ccd533" />
        <meta name="alpari_partner" content="1210333" />
        <?php global $shortname; ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(); ?> | <?php bloginfo('name'); ?></title>
        <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/css/landing-page.css" />
        <link href="http://fonts.googleapis.com/css?family=PT+Sans&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css" />
        <?php wp_head(); ?>
        <style>
            html{margin-top:0!important;}
        </style>
    </head>
<body data-spy="scroll" data-target=".navbar-collapse" data-pinterest-extension-installed="cr1.39.1">
    <div class="page-wrapper">
        <header id="main-header" class="main-header image-header">
            <div class="color-overlay">
                <div class="container">
                    <div class="header-content row">
                        <?php if(have_posts()): the_post(); ?>
                            <style>
                                body {background: url("<?php the_field('background_image'); ?>") center center / cover no-repeat white }
                                .main-header .hilite, .main-header strong, .main-header b{color:<?php the_field("basic_color"); ?>!important}
                            </style>

                        <div class="col-sm-6">
                            <h1><?php echo strip_tags(get_field('header'), "<a><b><strong><u><i>"); ?></h1>
                            <?php the_content(); ?>
                            <br>
                            <a class="scrollto" href="<?php the_field('download_link'); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/App_Store_Logo.png" alt="Скачать приложение c Apple Store" class="img"/>
                            </a>
                        </div>

                        <div class="col-sm-6 pos-relative">
                            <div class="phones-wrap">
                                <img src="<?php the_field('application_picture'); ?>" class="mockup mockup-main" alt="">
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter21436993 = new Ya.Metrika({id:21436993,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
</body>
</html>