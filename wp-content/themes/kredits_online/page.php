<?php get_header(); ?>
		<?php $args = array(
			'theme_location'  => 'fine-menu',
			'container'       => 'nav',
			'container_class' => 'block_nav',
			'menu_class'      => 'menu',
			'echo'            => true,  
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 1 );
		wp_nav_menu($args); ?>	
		<section class="content">
			<section class="content_wrapp">
				<section class="single_area">
					<?php if(have_posts()): the_post(); ?>
						<h1 class="page_title"><?php the_title(); ?></h1>
				                <?php
							$show_thumb = get_post_meta(get_the_ID(),'kredits_display_thumb');
							if (!empty($show_thumb)){
								echo get_the_post_thumbnail(get_the_ID(), 'thumb-size-4',
									array('alt' => get_the_title(),'title' => get_the_title(), 'class' => 'single_image')
								);}
						?>
						<?php the_content(); ?>
					<?php endif; ?>
				</section>
				<section class="single_area">
					<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus,lj"></div> 
				</section>
				<section class="single_area"></section>
			</section>
			<?php get_sidebar(); ?>
		</section>
	</div>
<?php get_footer(); ?>