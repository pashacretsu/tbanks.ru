<?php get_header(); ?>
<?php $post = $wp_query->post; if (is_single() && in_category( '306' ) ): ?>
<div id="content">
	<div class="content">
		<div class="link_go_page">
			<?php if(have_posts()): the_post(); ?>
			<center>
				<h2>Сейчас Вы будете перенаправлены на сайт банка</h2>
			</center>
			<br />
			<center>
				<?php
					echo get_the_post_thumbnail(get_the_ID(), 'thumb-size-6',
						array('alt' => get_the_title(),'title' => get_the_title(), 'class' => 'single_image')
					);
				?>
			</center>
			<br />
			<div class="blb_more1">
				<center>
					Если перенаправление не произошло в течение 
					<span id="time">3</span> 
					секунд, 
					<a href="<?php the_field("link"); ?>">нажмите здесь</a>
					<script>
						(function ($) {
							jQuery(window).load(function(){
								setTimeout(function(){
									jQuery(".link_go_page #time").text("2");
								}, 1000);
								setTimeout(function(){
									jQuery(".link_go_page #time").text("1");
								}, 2000);
								setTimeout(function(){
									window.location.href = "<?php the_field("link"); ?>";
									jQuery(".link_go_page #time").text("0");
								}, 3000);
							});
						})(jQuery);
					</script>
				</center>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php else: ?>
		<?php $args = array(
			'theme_location'  => 'fine-menu',
			'container'       => 'nav',
			'container_class' => 'block_nav',
			'menu_class'      => 'menu',
			'echo'            => true,  
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 1 );
		wp_nav_menu($args); ?>	
		<section class="content">
			<section class="content_wrapp">
				<section class="single_area">
					<?php if(have_posts()): the_post(); ?>
						<h1 class="page_title"><?php the_title(); ?></h1>
						<?php
							$show_thumb = get_post_meta(get_the_ID(),'kredits_display_thumb');
							if (!empty($show_thumb)){
								echo get_the_post_thumbnail(get_the_ID(), 'thumb-size-4',
									array('alt' => get_the_title(),'title' => get_the_title(), 'class' => 'single_image')
								);}
						?>
<?php the_content(); ?>
					<?php endif; ?>

				</section>

<section class="related_post">
					<h2>Также рекомендуем:</h2>
					<?php 
						$categories = wp_get_post_categories(get_the_ID());
						$cats = implode(',', $categories);
						$query = array( 
							'numberposts' => 4,
							'exclude' => get_the_ID(),
							'category' => $cats,
							'post_status' => 'publish',
							'post_type' => 'post'
						);
						$fresh_posts = get_posts($query);
						$i=1;
						foreach( $fresh_posts as $post ): ?>
							<article class="post">
								<a class="post_image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<?php if ( has_post_thumbnail()) {
										the_post_thumbnail('thumb-size-5',
											array('alt' => get_the_title())
										);} else { echo '<img src="'.get_template_directory_uri().'/images/thumbs/145x99.png" alt="'.get_the_title().'"/>';}
									?>
								</a>
								<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
							</article>
					<?php $i++; endforeach; wp_reset_query(); ?>
				</section>
                <section class="single_area">
				    <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="button" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus,lj"></div>
				</section>
				<section class="single_area"></section>
			</section>
			<?php get_sidebar(); ?>
		</section>
	</div>
<?php get_footer(); ?>
<?php endif; ?>