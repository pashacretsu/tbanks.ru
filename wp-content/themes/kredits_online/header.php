<!DOCTYPE html>
<html lang="ru">
<head>
<meta name="cmsmagazine" content="0fb1a2d032e6e4d535f6138629ccd533" />
        <meta name="alpari_partner" content="1210333" />
	<?php global $shortname; ?>
	<meta charset="utf-8">
	<title><?php wp_title(); ?> | <?php bloginfo('name'); ?></title>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link href="http://fonts.googleapis.com/css?family=PT+Sans&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css" />
	<?php if(is_page_template("template-calc.php")): ?>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/gcTools.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/newCredit.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/groupNums3.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/timers.js"></script>
	<?php endif; ?>
	<script type="text/javascript">
		(function($){
			$(function(){
				$('select').styler();
			})
		})(jQuery)
	</script>
	<?php wp_head(); ?> 
</head>
<body>


<?php
	$show = true;
	if(is_single() && in_category('go')) $show = false;
?>



<?php if ($show): ?>
	<div class="wrapper">
		<header>
			<a href="<?php bloginfo('url') ?>" title="<?php bloginfo('name'); ?>" class="logo">
				<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo('name'); ?>">
			</a>
				<div class="top_banner">
<!--==========================================================================================================================-->
<a href="https://itunes.apple.com/ru/app/zajmy-onlajn/id1135588579?mt=8" target='_blank'><img src="http://tbanks.ru/wp-content/uploads/2016/12/602.png" alt="Приложение "Микрозаймы"" width="600" height="90" border="0" /></a>


<!--==========================================================================================================================-->
</div>
			<div class="navigation">
				<div class="border">
					<?php $args = array(
						  'theme_location'  => 'main-menu',
						  'container'       => 'nav',
						  'menu_class'      => 'menu',
						  'echo'            => true,  
						  'fallback_cb'     => 'wp_page_menu',
						  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						  'depth'           => 1,
						);
					wp_nav_menu($args); ?>
					<div class="city">
						<select title="Выберите ваш город" id="cities-list">
							<option>Выберите ваш город</option>
							<?php
								$args = array(
									"post_type" => "post",
									"numberposts" => -1,
									'post_status' => 'publish',
									"cat" => 307,
									"meta_key" => "search_title",
									"orderby" => "meta_value",
									"order" => "asc"
								);
								$pages = get_posts($args);
								if ($pages) {
									foreach ($pages as $page){ ?>
										<option value="<?php echo get_permalink($page->ID); ?>"><?php the_field("search_title", $page->ID); ?></option>
									<?php }
								}
							?>
						</select>
					</div>
					<div class="search_form">
						<form action="<?php bloginfo('url') ?>" method="get" role="search">
							<input type="text" title="Поиск по сайту" name="s" value="<?php if(!empty($_GET['s'])):echo $_GET['s']; else: echo 'Поиск по сайту'; endif; ?>" />
							<input type="submit" value="" />
						</form>
					</div>
				</div>
			</div>
		</header>
<?php endif; ?>