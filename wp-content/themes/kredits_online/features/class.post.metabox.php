<?php

class postMetaBox{

	public $meta_fields;
	private $users;

	function __construct(){
		$this->default_init();
		add_action('add_meta_boxes', array(&$this, 'add_custom_meta_box'));
		add_action('save_post', array(&$this, 'save_custom_meta'));
		add_action('admin_init', array(&$this, 'mytheme_add_init'));
	}

	public function add_custom_meta_box() {
		add_meta_box(
			'custom_meta_box',
			'Дополнительные настройки',
			array(&$this,'show_custom_meta_box'),
			'post',
			'side'
		);
	}

	private function default_init() {
		$this->meta_fields = array(			
			array (  
			    'label'	=>	'Отображать миниатюру в статье?',
			    'id'	=>	'kredits_display_thumb',
			    'type'	=>	'checkbox',
			    'std'	=>	''
			),	
		);
		return true;
	}

	public function show_custom_meta_box(){
	?>
	<style>
		.tab_1,.tab_2,.tab_3,.tab_4{display:none}
		.blockImp{display:block !important}
	</style>
	<div style="position:relative;width:100%;overflow:hidden">
	<form action="" method="post">
		<input type="hidden" name="custom_meta_box_nonce" value="<?php echo  wp_create_nonce(basename(__FILE__)); ?>" />
		<?php 
		foreach ($this->meta_fields as $field){
			$meta = get_post_meta(get_the_ID(), $field['id']);
			if($field['type'] == 'hidden'){
				$empty_val = (!empty($meta))?$meta[0]:0;
				echo '<input type="hidden" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$empty_val.'"/>';
			}
		}
		$tem = get_post_meta(get_the_ID(), 'cpost_template');
		$template = 'tab_'.$tem[0];
		foreach ($this->meta_fields as $field){
			$meta = get_post_meta(get_the_ID(), $field['id']);
			switch($field['type']) {
				case 'devider':
					if($template == $field['class']) $d = 'blockImp'; else $d = "";
					echo '<hr class="data_fld '.$d.' '.$field['class'].'" style="position:relative;float:left;width:100%;margin:5px 0"/>';
					break;
				case 'textarea':
					if($template == $field['class']) $d = 'blockImp'; else $d = "";
					echo '<p class="data_fld '.$d.' '.$field['class'].'" style="text-align:right;position:relative;float:left;width:100%;margin:5px 0"><label style="float:left" for="'.$field['id'].'">'.$field['label'].'</label>';
					$_ = get_post_meta(get_the_ID(),$field['id']);
					$current = (string)$_[0];
					$vall = (!empty($current)) ? $current : '';
					echo '<textarea style="width:255px;height:150px;float:left" name="'.$field['id'].'" id="'.$field['id'].'">'.$vall.'</textarea>';
					echo '</p>';
					break;
				case 'text':
					if($template == $field['class']) $d = 'blockImp'; else $d = "";
					echo '<p class="data_fld '.$d.' '.$field['class'].'" style="text-align:right;position:relative;float:left;width:100%;margin:5px 0"><label style="float:left" for="'.$field['id'].'">'.$field['label'].'</label>';
					$_ = get_post_meta(get_the_ID(),$field['id']);
					$current = (string)$_[0];
					$vall = (!empty($current)) ? $current : '';
					echo '<input type="text" style="width:255px;float:left" value="'.$vall.'" name="'.$field['id'].'" id="'.$field['id'].'" /><br/>';
					echo '</p>';
					break;
				case 'checkbox':
					if($template == $field['class']) $d = 'blockImp'; else $d = "";
					echo '<p class="data_fld '.$d.' '.$field['class'].'" style="text-align:right;position:relative;float:left;width:100%;margin:5px 0"><label style="float:left" for="'.$field['id'].'">'.$field['label'].'</label>';
					$_ = get_post_meta(get_the_ID(),$field['id']);
					$current = (string)$_[0];
					$vall = (!empty($current)) ? 'checked="checked"' : '';
					echo '<input type="checkbox" '.$vall.' name="'.$field['id'].'" id="'.$field['id'].'" /><br/>';
					echo '</p>';
					break;
				case 'select':
					if($template == $field['class']) $d = 'blockImp'; else $d = "";
					echo '<p class="data_fld '.$d.' '.$field['class'].'" style="text-align:right;position:relative;float:left;width:100%;margin:5px 0"><label style="float:left" for="'.$field['id'].'">'.$field['label'].'</label>';
					$_ = get_post_meta(get_the_ID(),$field['id']);
					$current = (string)$_[0];
					echo '<select style="width: 120px;" '.$vall.' name="'.$field['id'].'" id="'.$field['id'].'" />';
					foreach ($field['optns'] as $key => $opt){
						if($current == $key) $res = ' selected="selected"'; else $res = '';
						echo "<option value='".$key."'".$res.">".$opt."</option>";
					}
					echo '</select></p>';
					break;
			}
		}
		?>
	</form>
	</div>
	<?php
	}
	
	
	public function mytheme_add_init(){
	
	}
	
	

	public function save_custom_meta($post_id){
		if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__)))
			return $post_id;
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
			return $post_id;
		if ('page' == $_POST['post_type']){
			if (!current_user_can('edit_page', $post_id)){
				return $post_id;
			}
		} elseif (!current_user_can('edit_post', $post_id)){
			return $post_id;
		}
		foreach ($this->meta_fields as $field){
			$old = get_post_meta($post_id, $field['id'], true);
			$new = $_POST[$field['id']];
			if(!old){
				add_post_meta($post_id, $field['id'], $old);
			}elseif ($new && $new != $old){
				update_post_meta($post_id, $field['id'], $new);
			} elseif ('' == $new && $old){
				delete_post_meta($post_id, $field['id'], $old);
			}
		}
	}
}

$postMetaBox = new postMetaBox();