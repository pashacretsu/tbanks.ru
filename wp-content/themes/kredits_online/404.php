<?php get_header(); ?>

		<?php $args = array(
			'theme_location'  => 'fine-menu',
			'container'       => 'nav',
			'container_class' => 'block_nav',
			'menu_class'      => 'menu',
			'echo'            => true,  
			'fallback_cb'     => 'wp_page_menu',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 1 );
		wp_nav_menu($args); ?>	
		<section class="content">
			<section class="content_wrapp">
				<section class="single_area">
					<h1 class="page_title">Ошибка 404</h1>
					<p>Страница не найдена</p><img src="http://tbanks.ru/wp-content/uploads/2013/11/2.png" alt="404" style="border: none;" /></a>
				</section>
			</section>
			<?php get_sidebar(); ?>
		</section>
	</div>
<?php get_footer(); ?>