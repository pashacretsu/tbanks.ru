<?php

require_once(ABSPATH . 'wp-settings.php');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php' );


class iOSOptions {

    public $tableName;

    private $wpDBContext;

    private $socketConnection;

    private $devicesURLKey;

    private $settingsURLKey;

    private $pemFileExtension = "pem";

    private $prefix;

    private $appplicationName;

    private $pemFileOptionKey;

    private $pemFilePassphraseOptionKey;

    private $customPushNotificationText;

    private $pemPushEnvironmentnKey;

    private $pemFilePassphraseDefaultValue;

    private $postType;

    private $testDevice = null; //fa72d4025b6dbe5bae958f24df59a7c8fe6c2df329a0c5f1a23a831ce469cc56

    //ssl://gateway.push.apple.com:2195          //live
    //ssl://gateway.sandbox.push.apple.com:2195  //demo
    private $pemPushEnvironmentnDefaultValue;


    function __construct($prefix, $appplicationName) {

        global $wpdb;

        $this->prefix = $prefix;
        $this->postType = $this->prefix . "-" . ZUPostTypeName;
        $this->settingsURLKey = $this->getPrefix() . "config";
        $this->devicesURLKey = $this->getPrefix() . "devices";
        $this->appplicationName = $appplicationName;
        $this->pemFileOptionKey = $this->prefix . "_pem_file_option_key";
        $this->pemPushEnvironmentnKey = $this->prefix . "_push_environment_option_key";
        $this->pemFilePassphraseOptionKey = $this->prefix . "_pem_file_passphrase_option_key";
        $this->customPushNotificationText = $this->prefix . "_custom_push_notification_text";
        $this->pemFilePassphraseDefaultValue = "";
        $this->pemPushEnvironmentnDefaultValue = 'ssl://gateway.push.apple.com:2195'; //live by default

        $this->tableName = $wpdb->prefix . "uuid_items";
        $this->wpDBContext = $wpdb;
    }


    private function getPrefix() {

        return ($this->prefix) ? $this->prefix."-" : "";

    }



    public function getPassPhrase() {

        return (get_option($this->pemFilePassphraseOptionKey)) ? get_option($this->pemFilePassphraseOptionKey) : $this->pemFilePassphraseDefaultValue;

    }



    public function getSslPath() {

        return (get_option($this->pemPushEnvironmentnKey)) ? get_option($this->pemPushEnvironmentnKey) : $this->pemPushEnvironmentnDefaultValue;

    }



    public function getPemFileName() {

        return get_option($this->pemFileOptionKey);

    }



    private function getPemFileDir() {

        $folderPath = Wordpress_iOS_Plugin_Folder . "/pem/" .$this->prefix."/";

        if (!file_exists($folderPath)) {
            mkdir($folderPath, 0777, true);
        }

        return $folderPath;
    }



    public function getPemFileFullPath() {

        $fullFilePath = $this->getPemFileDir() . $this->getPemFileName();

        return ($this->getPemFileName() && (file_exists($fullFilePath))) ? $fullFilePath : null;

    }



    public function updatePluginOptionsWithPostData($postData, $fileData) {

        update_option($this->pemFilePassphraseOptionKey, esc_attr($postData[$this->pemFilePassphraseOptionKey]));

        update_option($this->pemPushEnvironmentnKey, esc_attr($postData[$this->pemPushEnvironmentnKey]));

        (get_option($this->pemFilePassphraseOptionKey)) ? get_option($this->pemFilePassphraseOptionKey) : $this->pemFilePassphraseDefaultValue;

        if ($fileData[$this->pemFileOptionKey]['size']) {

            $filename = $fileData[$this->pemFileOptionKey]['name'];

            if( pathinfo($filename, PATHINFO_EXTENSION) != $this->pemFileExtension ) {

                wp_die("Не верный формат файла");

            }

            if (file_exists($this->getPemFileFullPath())) {

                unlink($this->getPemFileFullPath());

            }

            update_option($this->pemFilePassphraseOptionKey, esc_attr($postData[$this->pemFilePassphraseOptionKey]));

            $target_path = $this->getPemFileDir(). basename( $filename );

            if(move_uploaded_file($fileData[$this->pemFileOptionKey]['tmp_name'], $target_path)) {

                update_option($this->pemFileOptionKey, esc_attr($filename));

            } else {

                delete_option($this->pemFileOptionKey);

            }
        }
    }



    public function sendCustomPushWithPostData($postData) {

        $pushMessage = esc_attr($postData[$this->customPushNotificationText]);

        $this->sendPushNotificationWithText(0, $pushMessage, 2);
    }



    public function linkCustomStylesAndScriptsToDashboard() {

        if ($_GET['page'] && $_GET['page'] == $this->devicesURLKey) {

            wp_enqueue_style('data-table-css',          plugins_url( 'css/jquery.dataTables.min.css', Wordpress_iOS_Plugin_FILE ) );
            wp_enqueue_style('data-table-css-sh-core',  plugins_url( 'css/dataTables.bootstrap.min.css', Wordpress_iOS_Plugin_FILE ));
            wp_enqueue_style('font-awesome',  plugins_url( 'css/font-awesome.min.css', Wordpress_iOS_Plugin_FILE ));
            wp_enqueue_script('data-table-jquery',     plugins_url( 'js/jquery-1.12.0.min.js', Wordpress_iOS_Plugin_FILE ));
            wp_enqueue_script('data-table-js',         plugins_url( 'js/jquery.dataTables.min.js', Wordpress_iOS_Plugin_FILE ));
        }
    }



    private function createUUIDTable() {

        if($this->wpDBContext->get_var("show tables like '{$this->tableName}'") != $this->tableName) {
            $sql = "CREATE TABLE {$this->tableName} (
                    `id` mediumint(9) NOT NULL AUTO_INCREMENT,
                    `token` varchar(100) DEFAULT '' NOT NULL,
                    `uuid` varchar(255) DEFAULT '' NOT NULL,
                    `device` varchar(255) DEFAULT '' NOT NULL,
                    `version` varchar(255) DEFAULT '' NOT NULL,
                    `name` varchar(255) NOT NULL,
                    `app` varchar(255) NOT NULL,
                    `os` varchar(20) NOT NULL,
                    `createdAt` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    `updatedAt` datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    UNIQUE KEY id (id)
                    );";
            dbDelta( $sql );
        }
    }



    private function strReplaceAssoc(array $replace, $subject) {

        return str_replace(array_keys($replace), array_values($replace), $subject);

    }



    public function createNewUUID($_uuid, $_token, $_device, $_name, $_version, $_bundle, $_os = 'ios') {

        $replaceDict = array(
            '<' => '',
            '>' => '' ,
            ' ' => ''
        );

        $uuid = mysql_real_escape_string($_uuid);

        $token = mysql_real_escape_string($this->strReplaceAssoc($replaceDict, $_token));

        $device = mysql_real_escape_string($_device);

        $version = mysql_real_escape_string($_version);

        $name = mysql_real_escape_string($_name);

        $bundle = mysql_real_escape_string($_bundle);

        $os = mysql_real_escape_string($_os);

        if (!strlen($uuid) || !strlen($device) || !strlen($name) || !strlen($bundle)) {
            return;
        }

        $dbUUIData = $this->wpDBContext->get_row("SELECT id FROM ".$this->tableName." WHERE uuid = '".$uuid."' AND app = '".$bundle."'");

        $input = array(
            'updatedAt' => current_time( 'mysql' ),
            'token'     => $token,
            'uuid'      => $uuid,
            'device'    => $device,
            'version'   => $version,
            'name'      => $name,
            'app'       => $bundle,
            'os'        => $os
        );

        if (!$dbUUIData) {
            $input["createdAt"] = current_time( 'mysql' );

            $r = $this->wpDBContext->insert(
                $this->tableName,
                $input
            );

            echo '<pre>';
            var_dump($input);
            var_dump($r);
            echo '</pre>';
            die;

        } else {

            $this->wpDBContext->update(
                $this->tableName,
                $input,
                array( 'id' => $dbUUIData->id )
            );
        }
    }



    public function getUUIDCollection() {

        return $this->wpDBContext->get_results("SELECT * FROM " . $this->tableName." WHERE app = '".$this->prefix."'");

    }



    public function sendPushNotificationWithText($postID, $message, $code = 1) {

        $allDevices = $this->getUUIDCollection();

        if (!empty($this->testDevice)) {

            $this->sendPushToDevice($postID, $this->testDevice, $message, $code);

        } else {

            if (count($allDevices) > 0) {
                $iOSDevices = array();      //Array of device objects
                $androidDevices = array();  //Array of registered tokens

                foreach ($allDevices as $device) {
                    if ($device->os == "ios") {
                        $iOSDevices[] = $device;
                    } else {
                        $androidDevices[] = $device->token;
                    }
                }

                $this->sendPushToIOSDevice($postID, $iOSDevices, $message, $code);
                $this->sendPushToAndroidDevice($postID, $message, $androidDevices);
            }
        }
    }



    private function sendPushToAndroidDevice($postID = 0, $message, $registrationIDs) {
        if (!count($registrationIDs)) { return; }

        $message         = iconv(mb_detect_encoding($message), "UTF-8", $message);
        $serverToken     = 'AAAA521hjRo:APA91bFDEeuJ_GohO0G2grU6mP9xpGAKAIh0vV9OZRuty5TKffOSAcb9whIkV4GhjUMKB40uiBi5md4YePALMZMDGIo9wOZHpBT5g8R86NqLjyjk4DZrelasY-tenltEZ4qMSCQoh-zX';

        $fcmMsg = array(
            'body'  => $message
        );

        if ($postID > 0) {
            $fcmMsg['post'] = (int)$postID;
        }

        $fcmFields = array(
            'registration_ids' => json_encode($registrationIDs),
            'priority'         => 'high',
            'notification'     => $fcmMsg
        );

        $headers = array(
            'Authorization: key=' . $serverToken,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fcmFields));
        $result = json_decode(curl_exec($ch));

        curl_close($ch);
    }



    private function sendPushToIOSDevice($postID = 0, array $devices = array(), $message, $code = 1) {
        if ($this->getPemFileFullPath() && $this->getPassPhrase()) {
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $this->getPemFileFullPath());
            stream_context_set_option($ctx, 'ssl', 'passphrase', $this->getPassPhrase());

            $this->socketConnection = stream_socket_client($this->getSslPath(), $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$this->socketConnection) {
                exit("Failed to connect: $err $errstr" . PHP_EOL);
            }
        }

        $message = iconv(mb_detect_encoding($message), "UTF-8", $message);

        $body['aps'] = array(
            'content-available' => 1,
            'alert' => $message,
            'sound' => 'default',
            'code'  => $code,
            'badge' => 0
        );

        if ($postID > 0) {
            $body['aps']['post'] = (int)$postID;
        }

        $payload = json_encode($body);

        foreach ($devices as $device) {

            $msg = chr(0) . pack('n', 32) . pack('H*', $device->token) . pack('n', strlen($payload)) . $payload;

            $result = fwrite($this->socketConnection, $msg, strlen($msg));
        }

        fclose($this->socketConnection);
    }



    public function initUUIDManagerConfigurationPage() {
        add_submenu_page( 'edit.php?post_type='.$this->postType, 'Устройства приложения '.$this->appplicationName, 'Устройства', 'manage_options', $this->devicesURLKey, array( &$this, 'pushNotificationDevicesPage' ) );
        add_submenu_page( 'edit.php?post_type='.$this->postType, 'Push-уведомления приложения '.$this->appplicationName, 'Push-notifications', 'manage_options', $this->settingsURLKey, array( &$this, 'pushNotificationSettingPage' ) );
    }



    public function pushNotificationDevicesPage() {

        $UUIDs = $this->getUUIDCollection();

        $resultTable = "<div class='wrap'><h1>Устройства приложения ".$this->appplicationName."</h1><br/>";

        $resultTable .= "<table id='uuid-table' class='display table table-striped' cellspacing='0' width='100%'>";

        $resultTable .= "<thead><tr><th>Имя</th><th>Тип</th><th>Версия</th><th>Токен</th><th>Дата обновления</th><th></th></tr></thead>";

        $resultTable .= "<tbody>";

        if (count($UUIDs) > 0) {

            foreach ( $UUIDs as $item ) {

                $resultTable .= "<tr>
                <td class='td-name'>{$item->name}</td>
                <td class='td-device'>{$item->device}</td>
                <td class='td-version'>{$item->version}</td>
                <td class='td-uuid'>{$item->token}</td>
                <td class='td-updated-time'>{$item->updatedAt}</td>
                <td class='td-actions'><button class='delete-device-btn' data-uuid='{$item->uuid}'><i class='fa fa-chain-broken' aria-hidden='true'></i></button></td>
                </tr>";

            }
        }

        $resultTable .= "<tbody></table>";

        $resultTable .= "
        <script>
            $(document).ready(function() {
                $('#uuid-table').DataTable();

                $('#uuid-table').on('click', '.delete-device-btn', function(){

                    console.log('delete row!!!');

                    var deviceUUID = $(this).data('uuid'),
                        parentTR = $(this).closest('tr'),
                        ajaxUrl = '".admin_url('admin-ajax.php')."';

                    $.ajax({
                        type: 'GET',
                        url: ajaxUrl,
                        data: {
                            action: 'deletetoken',
                            prefix: '".$this->prefix."',
                            uuid :  deviceUUID
                        },
                        success: function (response) {
                            console.log(response);
                            parentTR.remove();
                        }
                    });
                })
            } );
        </script></div>";

        echo $resultTable;
    }



    public function deletetoken_action_callback() {

        $action = $_GET['action'];

        if ($action == 'deletetoken' && $_GET['prefix'] == $this->prefix) {

            $uuid = $_GET['uuid'];

            $uuidArr = array('uuid' => $uuid, 'app' => $this->prefix);

            $this->wpDBContext->delete($this->tableName, $uuidArr);

            print json_encode($uuidArr);
        }

        return;
    }



    public function pushNotificationSettingPage() {
        ?>
        <div class='wrap'>
            <h1>Настройки приложения <?php echo $this->appplicationName ?></h1><br/>
            <?php if( isset( $_GET[ 'tab' ] ) ) {
                $active_tab = $_GET[ 'tab' ];
            } 
            $isSendForm = ($active_tab == 'send' || $active_tab == '') ? true : false;
            $isSettingsForm = ($active_tab == 'settings') ? true : false;
            ?>
         
            <h2 class="nav-tab-wrapper">
                <a href="?post_type=<?php echo $this->postType; ?>&page=<?php echo $this->settingsURLKey; ?>&tab=send" class="nav-tab <?php echo ($isSendForm === true) ? 'nav-tab-active' : ''; ?>">Отправить</a>
                <a href="?post_type=<?php echo $this->postType; ?>&page=<?php echo $this->settingsURLKey; ?>&tab=settings" class="nav-tab <?php echo ($isSettingsForm === true) ? 'nav-tab-active' : ''; ?>">Настройки</a>
            </h2>
         
            <?php if ($isSendForm === true): ?>
                <form action="edit.php?post_type=<?php echo $this->postType; ?>&page=<?php echo $this->settingsURLKey; ?>" method="post">
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row">
                                <label for="<?php echo $this->customPushNotificationText; ?>">Push notification text</label>
                            </th>
                            <td>
                                <textarea style="width:320px;height:50px;resize:none" id="<?php echo $this->customPushNotificationText; ?>" name="<?php echo $this->customPushNotificationText; ?>"></textarea>
                                <p class="description" id="tagline-description">Длинна сообщения не должна превышать 50 символов</p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <input type="submit" value="Отправить" name="<?php echo $this->prefix ?>_send_custom_push" class="button button-primary button-large"/>
                            </th>
                            <td>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            <?php elseif ($isSettingsForm === true): ?>
                <form action="edit.php?post_type=<?php echo $this->postType; ?>&page=<?php echo $this->settingsURLKey; ?>&tab=settings" method="post" enctype="multipart/form-data">
                    <table class="form-table">
                        <tbody>
                        <tr>
                            <th scope="row">
                                <label for="<?php echo $this->pemFileOptionKey; ?>">Upload PEM file</label>
                            </th>
                            <td>
                                <input type="file"
                                       id="<?php echo $this->pemFileOptionKey; ?>"
                                       name="<?php echo $this->pemFileOptionKey; ?>"
                                    />
                                <input type="hidden" name="MAX_FILE_SIZE" value="4194304" />
                                <p class="description" id="tagline-description">
                                    Actual file: <?php echo $this->getPemFileFullPath(); ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label for="<?php echo $this->pemFilePassphraseOptionKey; ?>">Environment</label>
                            </th>
                            <td>
                                <?php $isLive = ($this->getSslPath() == $this->pemPushEnvironmentnDefaultValue); ?>
                                <select id="<?php echo $this->pemPushEnvironmentnKey; ?>" name="<?php echo $this->pemPushEnvironmentnKey; ?>" style="width:250px">
                                    <option<?php echo ($isLive) ? " selected" : ""; ?> value="ssl://gateway.push.apple.com:2195">Live</option>
                                    <option<?php echo (!$isLive) ? " selected" : ""; ?> value="ssl://gateway.sandbox.push.apple.com:2195">Demo</option>
                                </select>
                                <p class="description" id="tagline-description"></p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label for="<?php echo $this->pemFilePassphraseOptionKey; ?>">PEM file Passphrase</label>
                            </th>
                            <td>
                                <input type="text"
                                       style="width:250px"
                                       id="<?php echo $this->pemFilePassphraseOptionKey; ?>"
                                       name="<?php echo $this->pemFilePassphraseOptionKey; ?>"
                                       value="<?php echo $this->getPassPhrase(); ?>"
                                    />
                                <p class="description" id="tagline-description"></p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <input type="submit" value="Сохранить" name="<?php echo $this->prefix ?>_update_ios_settings" class="button button-primary button-large"/>
                            </th>
                            <td>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            <?php endif; ?>
        </div>
    <?php }



    public function configureData() {
        $this->createUUIDTable();
        add_action( 'admin_menu', array( &$this, 'initUUIDManagerConfigurationPage' ) );
        add_action( 'admin_enqueue_scripts', array( &$this, 'linkCustomStylesAndScriptsToDashboard' ) );
        add_action( 'wp_ajax_deletetoken', array( &$this, 'deletetoken_action_callback' ));
//        add_action( 'wp_ajax_nopriv_deletetoken', array( &$this, 'deletetoken_action_callback' ));
    }
}

