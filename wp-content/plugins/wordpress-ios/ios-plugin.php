<?php

if (file_exists(dirname( __FILE__ ) . '/ios-taxonomy.php')) require_once( 'ios-taxonomy.php' );
if (file_exists(dirname( __FILE__ ) . '/ios-options.php')) require_once( 'ios-options.php' );
if (file_exists(dirname( __FILE__ ) . '/ios-rest-api-v1.php')) require_once('ios-rest-api-v1.php');


class iOSPlugin {

    private $appName;

    private $menuPosition;

    private $prefix;

    private $taxonomy;

    private $postType;


    function __construct($name, $prefix, $position, $filename = null) {
        $this->prefix = $prefix;
        $this->appName = $name;
        $this->menuPosition = $position;
        $this->postType = (($this->prefix) ? $this->prefix."-" : "").ZUPostTypeName;
        $this->taxonomy = (($this->prefix) ? $this->prefix."-" : "").ZUTaxonomyName;
    }


    public function setupEnvironment(){

        $tax = new iOSTaxonomy($this->prefix, $this->appName, $this->menuPosition);
        $tax->menuPosition = (int)$this->menuPosition;
        $tax->configureData();

        $uuidObj = new iOSOptions($this->prefix, $this->appName);
        $uuidObj->configureData();

        if ($_GET[iOSGETKeyUUID] && $_GET[iOSGETKeyToken] && $_GET[iOSGETKeyDevice] && $_GET[iOSGETKeyName] && $_GET[iOSGETKeyVersion]) {

            $prefix = (strlen($_GET[iOSGETKeyApp]) >= 2 && strlen($_GET[iOSGETKeyApp]) <= 3) ? $_GET[iOSGETKeyApp] : 'ios';

            $uuidObj->createNewUUID($_GET[iOSGETKeyUUID], $_GET[iOSGETKeyToken], $_GET[iOSGETKeyDevice], $_GET[iOSGETKeyName], $_GET[iOSGETKeyVersion], $prefix);
            echo json_encode(array("success" => true, "app" => $prefix, "token" => $_GET[iOSGETKeyToken]));
            die;
        }

        if (isset($_POST[$this->prefix."_update_ios_settings"])) {
            $uuidObj->updatePluginOptionsWithPostData($_POST, $_FILES);
        }

        if (isset($_POST[$this->prefix."_send_custom_push"])) {
            $uuidObj->sendCustomPushWithPostData($_POST);
        }
    }
}