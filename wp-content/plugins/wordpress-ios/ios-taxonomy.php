<?php

require_once(ABSPATH . 'wp-settings.php');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php' );


require_once( 'ios-options.php' );


class iOSTaxonomy {

    public $jsonContentFile;

    public $jsonNewContentFile;

    public $appplicationName;

    public $menuPosition;

    private $addCustomActionPriority = 10;

    private $addCustomActionAcceptArgs = 3;

    private $isPushEnabledKey;

    private $pushTitleTextKey;

    private $prefix = "";

    private $taxonomy;

    private $postType;


    function __construct($prefix, $appplicationName, $position) {

        $this->prefix = $prefix;

        $this->appplicationName = $appplicationName;

        $this->menuPosition = $position;

        $this->postType = $this->getPrefix().ZUPostTypeName;

        $this->taxonomy = $this->getPrefix().ZUTaxonomyName;

        $this->isPushEnabledKey = "send_push_notification";

        $this->pushTitleTextKey = "push_notification_text";

        $filename = ($this->prefix != "ios") ? $this->getPrefix() . "mobile.json" : "mobile.json";

        $this->jsonContentFile = ABSPATH . $filename;

        $this->jsonNewContentFile = ABSPATH . "api/" . $filename;

    }


    private function getPrefix() {

        return ($this->prefix) ? $this->prefix."-" : "";

    }


    public function initTaxonomyCustomStyles() {

        if ($_GET['taxonomy'] && $_GET['taxonomy'] == $this->taxonomy) {

            wp_enqueue_style('font-awesome', plugins_url( 'css/font-awesome.min.css', Wordpress_iOS_Plugin_FILE ) );

        }
    }


    public function initCustomPostTypeAndTaxonomy() {

        $labels = array(
            'name'               => 'Записи '.$this->appplicationName,
            'singular_name'      => 'Запись '.$this->appplicationName,
            'menu_name'          => $this->appplicationName,
            'name_admin_bar'     => 'Запись '.$this->appplicationName,
            'add_new'            => 'Добавить запись',
            'add_new_item'       => 'Новая запись',
            'new_item'           => 'Новая запись',
            'edit_item'          => 'Редактировать запись',
            'view_item'          => 'Просмотр записи',
            'all_items'          => 'Все записи',
            'search_items'       => 'Поиск записи',
            'parent_item_colon'  => 'Родительская запись:',
            'not_found'          => 'Записей не найдено',
            'not_found_in_trash' => 'Записей в корзине не найдено'
        );

        $args = array(
            'labels'             => $labels,
            'description'        => 'На сайте данный тип записи не будет отображаться.',
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => $this->postType ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => $this->menuPosition,
            'supports'           => array( 'title', 'editor', 'thumbnail' )
        );

        register_post_type( $this->postType, $args );

        $labels = array(
            'name'              => 'Раздел '.$this->appplicationName,
            'singular_name'     => 'Раздел '.$this->appplicationName,
            'search_items'      => 'Поиск разделов',
            'all_items'         => 'Все разделы',
            'parent_item'       => 'Родительский раздел',
            'parent_item_colon' => 'Родительский раздел',
            'edit_item'         => 'Редактировать раздел',
            'update_item'       => 'Редактировать раздел',
            'add_new_item'      => 'Новый раздел',
            'new_item_name'     => 'Новый раздел',
            'menu_name'         => 'Разделы',
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => $this->taxonomy ),
        );

        register_taxonomy( $this->taxonomy, array( $this->postType ), $args );

        flush_rewrite_rules();
    }


    public function updateCustomPostType( $post_id, $post, $update ) {

        if ($post->post_type != $this->postType) {
            return;
        }

       $isPushEnabled = (function_exists("get_field")) ? get_field($this->isPushEnabledKey, $post->ID) : false;

        if ($isPushEnabled) {

            $manager = new iOSOptions($this->prefix, $this->appplicationName);

            $postMeta = get_post_meta( $post->ID );

            $manager->sendPushNotificationWithText($post->ID, ($postMeta[$this->pushTitleTextKey][0]) ? $postMeta[$this->pushTitleTextKey][0] : "");
        }

        $this->regenerateJSONData();
    }


    public function updateCustomTaxonomy( $taxonomy ){

        $this->regenerateJSONData();

    }


    private function regenerateJSONData() {

        $jsonContent = array (
            "updatedAt" => date("Y-m-d H:i:s"),
            "content"   => $this->getTaxonomiesContent()
        );

        $file = fopen($this->jsonContentFile,"w");

        fwrite($file, json_encode($jsonContent));

        fclose($file);



        // Generate new content for refactored app
        $file = fopen($this->jsonNewContentFile,"w");

        fwrite($file, json_encode($this->getTaxonomiesContentNew()));

        fclose($file);
    }


    private function getTaxonomiesContent() {

        $iosCategories = get_terms(
            $this->taxonomy,
            array(
                'orderby'    => 'count',
                'hide_empty' => 0
            )
        );

        $jsonTerms = array();

        if (count($iosCategories) > 0) {

            foreach ($iosCategories as $category) {

                $postItems = get_posts(
                    array(
                        'post_type'     => $this->postType,
                        'numberposts'   => -1,
                        'tax_query'     => array(
                            array(
                                'taxonomy'  => $this->taxonomy,
                                'field'     => 'term_id',
                                'terms'     => $category->term_id
                            )
                        )
                    )
                );

                $jsonPosts = array();

                if (count($postItems) > 0) {

                    foreach ( $postItems as $post ) {

                        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 180, 100) );

                        $largeImage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 280, 156) );

                        $postMeta = get_post_meta( $post->ID );

                        $jsonPosts[] = array(
                            "bankid"        => $post->ID,
                            "title"         => $post->post_title,
                            "status"        => $post->post_status,
                            "url"           => get_permalink($post->ID),
                            "content"       => $post->post_content,
                            "thumb_url"     => (!$thumbnail[0]) ? "" : $thumbnail[0],
                            "image_url"     => (!$largeImage[0]) ? "" : $largeImage[0],
                            "max_amount"    => (float)($postMeta["max_money"][0]) ? $postMeta["max_money"][0] : "",
                            "term"          => ($postMeta["term"][0]) ? $postMeta["term"][0] : "",
                            "rate"          => ($postMeta["precentage"][0]) ? $postMeta["precentage"][0] : "",
                            "rating"        => (float)($postMeta["rate"][0]) ? $postMeta["rate"][0] : 0,
                            "referal_link"  => ($postMeta["referal_link"][0]) ? $postMeta["referal_link"][0] : "",
                            "money_method"  => ($postMeta["get_money_method"][0]) ? $postMeta["get_money_method"][0] : "",
                            "updated_at"    => $post->post_modified
                        );

                    }
                }

                $jsonTerms[] = array(
                    "title"       => $category->name,
                    "category_id" => $category->term_id,
                    "url"         => get_term_link($category),
                    "items"       => $jsonPosts,
                    "icon"        => (function_exists("get_field")) ? get_field('category_icon', $this->taxonomy."_".$category->term_id) : "",
                    "position"    => (function_exists("get_field")) ? (int)get_field('category_position', $this->taxonomy."_".$category->term_id) : 0
                );
            }
        }

        return $jsonTerms;
    }


    private function getTaxonomiesContentNew() {

        $iosCategories = get_terms(
            $this->taxonomy,
            array(
                'orderby'    => 'count',
                'hide_empty' => 0
            )
        );

        $jsonTerms = array();

        if (count($iosCategories) > 0) {

            foreach ($iosCategories as $category) {

                $postItems = get_posts(
                    array(
                        'post_type'     => $this->postType,
                        'numberposts'   => -1,
                        'tax_query'     => array(
                            array(
                                'taxonomy'  => $this->taxonomy,
                                'field'     => 'term_id',
                                'terms'     => $category->term_id
                            )
                        )
                    )
                );

                $jsonPosts = array();

                if (count($postItems) > 0) {

                    foreach ( $postItems as $post ) {

                        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 180, 100) );

                        $largeImage = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 280, 156) );

                        $jsonPosts[] = array(
                            "id"            => $post->ID,
                            "title"         => $post->post_title,
                            "url"           => (function_exists("get_field")) ? get_field("referal_link", $post->ID) : get_permalink($post->ID),
                            "content"       => $post->post_content,
                            "thumb_url"     => (!$thumbnail[0]) ? "" : $thumbnail[0],
                            "image_url"     => (!$largeImage[0]) ? "" : $largeImage[0],
                            "postmeta"      => $this->getPostmeta($post->ID)
                        );
                    }
                }

                $jsonTerms[] = array(
                    "title"    => $category->name,
                    "id"       => $category->term_id,
                    "items"    => $jsonPosts,
                    "icon"     => (function_exists("get_field")) ? get_field('category_icon', $this->taxonomy."_".$category->term_id) : "",
                    "position" => (function_exists("get_field")) ? (int)get_field('category_position', $this->taxonomy."_".$category->term_id) : 0
                );
            }
        }

        return $jsonTerms;
    }


    private function getPostmeta($postID) {
        $position = 0;
        $postmeta = array();

        if (function_exists("get_field")) {

            // Setting post raing from custom field
            $rating = (float)get_field("rate", $postID);
            $postmeta[] = array(
                "title"     => "Rating",
                "value"     => "{$rating}",
                "position"  => $position++,
                "id"        => "rating"
            );

            $isCountryEnabled = get_field("enable_adv_country", $postID);
            if ($isCountryEnabled == true) {
                $label = get_field("label_country", $postID);
                $value = implode(",", get_field("value_country", $postID));
                $postmeta[] = array(
                    "title"     => "{$label}",
                    "value"     => "{$value}",
                    "position"  => $position++,
                    "id"        => "country"
                );
            }

            // Getting aditional options if they are enabled
            for ( $i = 1; $i <= 3; $i++ ) {
                $isMetaEnabled = get_field("enable_adv_{$i}", $postID);
                if ($isMetaEnabled == true) {
                    $label = get_field("label_{$i}", $postID);
                    $value = get_field("value_{$i}", $postID);
                    $postmeta[] = array(
                        "title"     => "{$label}",
                        "value"     => "{$value}",
                        "position"  => $position++,
                        "id"        => "meta_{$i}"
                    );
                }
            }



            // Setting max money amount from custom field
            if ($max = get_field("max_money", $postID)) {

                $amount = (get_field("currency_sign", $postID) != null) ? $max." ".get_field("currency_sign", $postID) : $max;

                $postmeta[] = array(
                    "title"     => "max_money",
                    "value"     => "{$amount}",
                    "position"  => $position++,
                    "id"        => "max_money"
                );
            }

            // Setting term from custom field
            if ($term = get_field("term", $postID)) {
                $postmeta[] = array(
                    "title"     => "term",
                    "value"     => "{$term}",
                    "position"  => $position++,
                    "id"        => "term"
                );
            }

            // Setting precentage from custom field
            if ($precentage = get_field("precentage", $postID)) {
                $postmeta[] = array(
                    "title"     => "precentage",
                    "value"     => "{$precentage}",
                    "position"  => $position++,
                    "id"        => "precentage"
                );
            }
        }

        return $postmeta;
    }


    public function configureData() {

        add_action( 'init', array( &$this, 'initCustomPostTypeAndTaxonomy' ) );
        add_action( 'admin_menu', array( &$this, 'initTaxonomyCustomStyles' ) );
        add_action( 'save_post', array( &$this, 'updateCustomPostType' ), $this->addCustomActionPriority, $this->addCustomActionAcceptArgs );
        add_action( 'create_'.$this->taxonomy, array( &$this, 'updateCustomTaxonomy'), $this->addCustomActionPriority, $this->addCustomActionAcceptArgs );
        add_action( 'edit_'.$this->taxonomy, array( &$this, 'updateCustomTaxonomy'), $this->addCustomActionPriority, $this->addCustomActionAcceptArgs );
        add_action( 'delete_'.$this->taxonomy, array( &$this, 'updateCustomTaxonomy'), $this->addCustomActionPriority, $this->addCustomActionAcceptArgs );

    }
}