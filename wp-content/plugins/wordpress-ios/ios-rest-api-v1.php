<?php

require_once(ABSPATH . 'wp-settings.php');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php' );


class IosRestApi {

    private $pluginUrlName = "wp";

    private $pluginVersion = "v2";

    function __construct($_prefix) {
        $this->pluginUrlName = $_prefix;
    }


    public function setUp() {

        //flush_rewrite_rules();

        add_action( 'rest_url_prefix', array(&$this, 'initRestRoute'));

        add_action( 'rest_api_init', array(&$this, 'registerRoutes'));
    }


    public function initRestRoute() {
        return 'wp-api';
    }


    public function registerRoutes() {

        $namespace  = $this->pluginUrlName . "/" . $this->pluginVersion;

        register_rest_route($namespace, '/categories/', array(
            'methods'   => 'GET',
            'callback'  => array(&$this, 'getCategories'),
        ));

        register_rest_route($namespace, '/categories/(?P<id>\d+)', array(
            'methods'   => 'GET',
            'callback'  => array(&$this, 'getCategory'),
        ));

        var_dump($namespace);die;
    }


    public function getCategories() {
        return Array("Category 1", "Category 2", "Category 3");
    }


    public function getCategory( $data ) {

        return $data;
    }
}