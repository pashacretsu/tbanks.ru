<?php
/**
 * Plugin Name: Микрозаймы -> Wordpress iOS Application extension
 * Plugin URI: http://cretsu.name
 * Description: Customise iOS application content by WordPress native dashboard
 * Version: 0.0.1
 * Author: Pasha Cretsu
 * Author URI: http://cretsu.name
 * License: GPL
 * Copyright: Pasha Cretsu
 */



define( 'iOSGETKeyApp',    'app' );
define( 'iOSGETKeyUUID',   'UUIDKey' );
define( 'iOSGETKeyToken',  'newToken' );
define( 'iOSGETKeyName',   'name' );
define( 'iOSGETKeyVersion','applicationVersion' );
define( 'iOSGETKeyDevice', 'device' );
define( 'iOSGETKeyЩЫ', 'osKey' );
define( 'ZUTaxonomyName',  'category' );
define( 'ZUPostTypeName',  'item' );



if ( ! function_exists( 'add_filter' ) ) {
    header( 'Status: 403 Forbidden' );
    header( 'HTTP/1.1 403 Forbidden' );
    exit();
}



if ( ! defined( 'Wordpress_iOS_Plugin_FILE' ) ) {
    define( 'Wordpress_iOS_Plugin_FILE', __FILE__ );
}



if ( ! defined( 'Wordpress_iOS_Plugin_Folder' ) ) {
    define( 'Wordpress_iOS_Plugin_Folder', dirname( __FILE__ ) );
}



if (file_exists(dirname( __FILE__ ) . '/ios-plugin.php')) require_once( 'ios-plugin.php' );

class IosPluginnConfig {

    private $envs = Array(
        "ios"   =>  "Микрозаймы",
        "zu"    =>  "Займы Украины",
        "zo"    =>  "Займы Онлайн",
        "kz"    =>  "Кредиты и займы",
        "ml"    =>  "Microloans",
        "zm"    =>  "Займы",
        "dz"    =>  "Деньги до ЗП",
        "rm"    =>  "РубльМен",
        "ps"    =>  "Préstamos",
        "lo"    =>  "Loans online",
        "zk"    =>  "Займы в Казахстане",
        "zz"    =>  "Займы до зарплаты онлайн",
        "ln"    =>  "Laenud",
        "ky"    =>  "Kredyty",
        "el"    =>  "Euro loans",
        "zko"   =>  "Займы на карту онлайн",
        "zbo"   =>  "Займы без отказа онлайн",
        "pr"    =>  "Prêts"
    );

    public function init() {
        $i = 5000;
        foreach ($this->envs as $key=>$title) {
            $tax = new iOSPlugin($title, $key, $i++);
            $tax->setupEnvironment();
        }
    }
}

$config = new IosPluginnConfig();
$config->init();
